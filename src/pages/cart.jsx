import ProductList from "../components/cards/product-list";

export function Cart(props){
    return (
        <>
            <h1 style={{textAlign: 'center', color: '#007eb9'}}>Cart</h1>
            <ProductList
                products={props.products}
                cartItems={props.cartItems}
                favourites={props.favourites}
                onAddProductToCart={props.onAddProductToCart}
                onRemoveProductFromCart={props.onRemoveProductFromCart}
                onAddProductToFavs={props.onAddProductToFavs}
                onRemoveProductFromFavs={props.onRemoveProductFromFavs}  
                />
        </>
    )
}