import ProductList from "../components/cards/product-list";

export function Home(props){
    return (
        <>
            {/* <h1 style={{textAlign: 'center', color: '#007eb9'}}>Home page</h1> */}
            <ProductList
                products={props.products}
                cartItems={props.cartItems}
                favourites={props.favourites}
                onAddProductToCart={props.onAddProductToCart}
                onRemoveProductFromCart={props.onRemoveProductFromCart}
                onAddProductToFavs={props.onAddProductToFavs}
                onRemoveProductFromFavs={props.onRemoveProductFromFavs}
            />
        </>
    )
}