import './App.css';
import { Routes, Route } from 'react-router-dom';
import { useEffect, useState } from 'react';
import ProductList from './components/cards/product-list';
import Header from './components/header/header';
import { Favourites } from './pages/favourites';
import { Cart } from './pages/cart';
import { Home } from './pages/home';


export default function App() {
  const [products, setProducts] = useState([]);
  const [cartItems, setCartItems] = useState([]);
  const [favourites, setFavourites] = useState([]);

  useEffect(() => {
    fetch('/products.json')
      .then(response => response.json())
      .then(data => {
          return setProducts(data)
      });

      // get saved cart items from LC
      const cartItems = getItemsFromLocalStorage('cartItems');
      if (cartItems instanceof Array) {
        setCartItems(cartItems);
      }

      // get saved favourites items from LC
      const favourites = getItemsFromLocalStorage('favourites');
      if (favourites instanceof Array) {
        setFavourites(favourites);
      }
  }, []);

  useEffect(() => {
    saveItemsToLocalStorage('cartItems', cartItems);
  }, [cartItems]);

  useEffect(() => {
    saveItemsToLocalStorage('favourites', favourites);
  }, [favourites]);
  
  function getItemsFromLocalStorage(itemKey) {
    let items = [];

    try {
      items = JSON.parse(window.localStorage.getItem(itemKey) || '[]')
    } catch (e) {
      console.log('Inconsistent data in storage');
    }

    return items;
  }

  function saveItemsToLocalStorage(itemKey, items) {
    window.localStorage.setItem(itemKey, JSON.stringify(items));
  }

  function addProductToCart(productID) {
    let items = [...cartItems];
    if (items.indexOf(productID) < 0) {
      items.push(productID);
      setCartItems(items)
    }
  }

  function addProductToFavourites(productID){
    let items = [...favourites];
    if(items.indexOf(productID) < 0){
      items.push(productID);
      setFavourites(items)
    }
  } 

  function removeProductFromCart(productID) {
    setCartItems(cartItems.filter((item) => item !== productID))
  }

  function removeProductFromFavourites(productID) {
    setFavourites(favourites.filter((item) => item !== productID))
  }

  return (
    <>
      <Header cartCount={cartItems.length} favouriteCount={favourites.length}/>
      <Routes>
        <Route 
          path="/" 
          element={<ProductList 
            products={products}
            cartItems={cartItems}
            favourites={favourites}
            onAddProductToCart={(productID) => addProductToCart(productID)}
            onRemoveProductFromCart={(productID) => removeProductFromCart(productID)}
            onAddProductToFavs={(productID) => addProductToFavourites(productID)}
            onRemoveProductFromFavs={(productID) => removeProductFromFavourites(productID)}  
            />}
          />
          <Route 
          path="/home" 
          element={<Home
            products={products}
            cartItems={cartItems}
            favourites={favourites}
            onAddProductToCart={(productID) => addProductToCart(productID)}
            onRemoveProductFromCart={(productID) => removeProductFromCart(productID)}
            onAddProductToFavs={(productID) => addProductToFavourites(productID)}
            onRemoveProductFromFavs={(productID) => removeProductFromFavourites(productID)}  
            />}
          />
        <Route
          path="/cart" 
          element={<Cart 
            products={products.filter((product) => cartItems.indexOf(product.sku) >= 0)}
            cartItems={cartItems}
            favourites={favourites}
            onAddProductToCart={(productID) => addProductToCart(productID)}
            onRemoveProductFromCart={(productID) => removeProductFromCart(productID)}
            onAddProductToFavs={(productID) => addProductToFavourites(productID)}
            onRemoveProductFromFavs={(productID) => removeProductFromFavourites(productID)}
            />} 
          />
        <Route 
          path="/favourites"
          element={<Favourites
            products={products.filter((product) => favourites.indexOf(product.sku) >= 0)}
            cartItems={cartItems}
            favourites={favourites}
            onAddProductToCart={(productID) => addProductToCart(productID)}
            onRemoveProductFromCart={(productID) => removeProductFromCart(productID)}
            onAddProductToFavs={(productID) => addProductToFavourites(productID)}
            onRemoveProductFromFavs={(productID) => removeProductFromFavourites(productID)}
            />}
          />
      </Routes>
    </>
  )
}