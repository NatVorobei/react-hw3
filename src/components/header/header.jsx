import './header.scss';
import { NavLink } from "react-router-dom";

export default function Header(props) {
    const {cartCount, favouriteCount} = props;
    return (
        <>
            <div className="header_container">
                <div className="header_logo">
                    <NavLink to="/home"><img src="/logo.png" alt="Home" title="Home" width="400px"/></NavLink>
                </div>
                    <div className="header_actions">
                        <NavLink to="/cart" className="nav_link">
                            <div className="products_cart">
                                <img src="/shopping-cart.png" alt="cart" width="30px"/>
                                <span>{cartCount}</span>
                            </div>
                        </NavLink>
                        <NavLink to="/favourites" className="nav_link">
                            <div className="products_favourite">
                                <img src="/star.png" alt="star" width="30px"/>
                                <span>{favouriteCount}</span>
                            </div>
                        </NavLink>
                    </div>
                </div>
        </>
    )
}

