import Button from "../buttons";
import './product-card.scss';
import { StarNotSolid, StarSolid } from "../icons";

export default function ProductCard(props) {
    const {
        name, 
        price, 
        image, 
        sku, 
        color,  
        onAddToCart, 
        onRemoveFromToCart, 
        onAddToFavourite, 
        onRemoveFromFavourite, 
        isAddedToCart, 
        isAddedToFavourites
    } = props;

    

    return(
        <>
        <div id={'sku' + sku} className="product">
            {isAddedToCart ? (
                <button className="close_btn"onClick={onRemoveFromToCart}>×</button>
                ) : (
                    null
                ) 
            }

            <div className="product_image">
                <img className="prod_img" src={image} alt={name} width="300px" height="190px"/>
            </div>
            <div className="product_descr">
                <p className="product_title">{name}</p>
                <p>Color: {color}</p>
                <p>${price}</p>
            </div>
            <div className="product_btns">
                {isAddedToCart ? (
                    <Button className="card_btn" text="Remove from cart" onClick={onRemoveFromToCart} />
                ) : (
                    <Button className="card_btn" text="Add to cart" onClick={onAddToCart} />
                )}
                
                {isAddedToFavourites ? (
                    <StarSolid 
                        style={{cursor: 'pointer'}}
                        width={35}
                        height={35}
                        fill="#007eb9"
                        onClick={onRemoveFromFavourite}
                        />
                ) : (
                    <StarNotSolid
                        style={{cursor: 'pointer'}}
                        width={35}
                        height={35}
                        fill="#007eb9"
                        onClick={onAddToFavourite}
                        />
                )}
            </div>
        </div>
        </>
    )
}