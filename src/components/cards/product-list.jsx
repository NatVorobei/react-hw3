import './product-card';
import ProductCard from "./product-card";
import Modal from "../modals";
import { useState } from 'react';

export default function ProductList(props) {
    const [isModalOpened, setIsModalOpened] = useState(false);
    const [modalData, setModalData] = useState({});
    
    function openAddToCartModal(productID) {
        setIsModalOpened(true);
        setModalData({action: 'add', productID, text: 'This product will be added to your cart'});
    }    
    
    function openRemoveFromCartModal(productID) {
        setIsModalOpened(true);
        setModalData({action: 'remove', productID, text: 'This product will be removed from your cart'});
    }

    function addToFavs(productID){
        props.onAddProductToFavs(productID);
    }

    function removeFromFavs(productID){
        props.onRemoveProductFromFavs(productID);
    }

    function closeModal() {
        setIsModalOpened(false);
        setModalData({});
    }

    function submitModal() {
        if (modalData.action === 'add') {
            props.onAddProductToCart(modalData.productID)
        } else if (modalData.action === 'remove') {
            props.onRemoveProductFromCart(modalData.productID)
        }
        closeModal();
    }
    
    return (
        <>
            <div className="products">
                {props.products.map(product => (
                    <ProductCard
                        key={product.id}
                        name={product.name}
                        price={product.price}
                        image={product.image}
                        sku={product.sku}
                        color={product.color}
                        isAddedToCart={props.cartItems.indexOf(product.sku) >= 0}
                        isAddedToFavourites={props.favourites.indexOf(product.sku) >= 0}
                        onAddToCart={() => openAddToCartModal(product.sku)}
                        onRemoveFromToCart={() => openRemoveFromCartModal(product.sku)}
                        onAddToFavourite={() => addToFavs(product.sku)}
                        onRemoveFromFavourite={() => removeFromFavs(product.sku)}
                    />
                ))}
            </div>
            <Modal
                show={isModalOpened}
                text={modalData.text} 
                onClose={() => closeModal()} 
                onSubmit={() => {submitModal()}} 
                />
        </>
    );
}

